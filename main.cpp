/* KeyLocker intrusion app
 * Copyleft CNED 2024
 */

#include "http_request.h"
#include "mbed.h"
#include "mbed_trace.h"
#include <cstring>

// bibliothèque des capteurs
#include "stm32l475e_iot01_accelero.h"

//Valeurs choisies pour l'échantillonage et l'envoi des données
#define SAMPLE_RATE 100 // Fréquence d'échantillonnage en Hz
#define N_SAMPLES 512    // Nombre d'échantillons
#define CHUNK_SIZE 64

// Blinking rate in milliseconds
#define BLINKING_RATE 5s


NetworkInterface *network;

// URL du web service
// Remplacer @IP pour l'adresse IP de votre serveur web

const char *add_test_sample_url = "http://192.168.1.174/m2-E6-5/api/add_sample.php";


const char *add_several_samples_url =
    "TO DO";

const char *add_acc_samples_url =
    "http://192.168.1.174/m2-E6-5/api/add_acc_samples.php";


/**
 * @fn void dump_response(HttpResponse *res)
 * @brief Fonction de lecture de la réponse HTTP du serveu
 * @param HttpResponse *res : L'objet HttpResponse à exploiter
 * @return void
 */
void dump_response(HttpResponse *res) {
  printf("Status: %d - %s\n", res->get_status_code(),
         res->get_status_message().c_str());

  printf("\nBody (%lu bytes):\n\n%s\n", res->get_body_length(),
         res->get_body_as_string().c_str());
}

/**
 * @fn int NetworkConnectMethod();
 * @brief Fonction de connexion au réseau (activation Wifi + dhcp)
 * @param aucun
 * @return int : 1 en cas d'echec, 0 success
 */
int NetworkConnectMethod() {
  printf("[NWK] Connecting to network...\n");
  network = NetworkInterface::get_default_instance();
  if (!network) {
    printf("[NWK] No network interface found, select an interface in "
           "'network-helper.h'\n");
    return 1;
  }
  nsapi_error_t err;
  if ((err = network->connect()) != NSAPI_ERROR_OK) {
    printf("[NWK] Failed to connect to network (%d)\n", err);
    return 1;
  }
  printf("[NWK] Connected to the network\n");
  SocketAddress ip;

  network->get_ip_address(&ip);
  printf("[NWK] IP address: %s\n", ip.get_ip_address());
  return 0;
}


/**
 * @fn void collectSamples(float samples2[])
 * @brief Méthode pour échantillonner les valeurs de l'accéléromètre sur la direction X
 * @param float samples2[] : le tableau des échantillons
 * @return void
 */
void collectSamples(float samples2[]) { 
  printf("\nCollect Samples\n");
  int16_t pDataXYZ[3];
  // Collecte des données
  for (int i = 0; i < N_SAMPLES; i++) {
      //TODO Collecter les données de l'accéléromètre
            // Conversion en g (de mg à g)
    samples2[i]= (float)rand() / RAND_MAX;
    ThisThread::sleep_for(chrono::milliseconds(1000 / SAMPLE_RATE));
  }
}

/**
 * @fn int httpTestSamplePostRequest(const char *url, float data)
 * @brief Méthode de test pour envoyer une donnée à l'API web service
 * @brief usage  : httpTestSamplePostRequest(add_test_sample_url, 20.5);
 * @param const char *url : l'url du web service
 * @param float data : La donnée
 * @return int : 1 en cas d'echec, 0 success
 */
int httpTestSamplePostRequest(const char *url, float data) {
  printf("\n----- HTTP POST request -----\n");

  HttpRequest *post_req = new HttpRequest(network, HTTP_POST, url);

  post_req->set_header("Content-Type", "application/x-www-form-urlencoded");

  // Ajout de la donnée à la requete
  char body[20];
  sprintf(body, "value=%0.2f", data);

  HttpResponse *post_res = post_req->send(body, strlen(body));
  if (!post_res) {
    printf("HttpRequest failed (error code %d)\n", post_req->get_error());
    return 1;
  }

  printf("\n----- HTTP POST response -----\n");
  dump_response(post_res);

  delete post_req;
  return 0;
}
 
/**
 * @fn int httpAddSamplesChunkPostRequestv1(const char *url, float data[], int offset, int chunkSize)
 * @brief Méthode pour envoyer un morceau de valeurs en JSON 
 * @brief Version 1 : Envoie les échantillons sous la forme {values : [data1, data2, data3, ..., dataN]}
 * @param const char *url : l'url du web service
 * @param float data[] : Le tableau de données
 * @param offset : la partie des données à envoyer
 * @param chunkSize : la taille des données à envoyer
 * @return void
 */
int httpAddSamplesChunkPostRequestv1(const char *url, float data[], int offset, int chunkSize) {
    printf("\n----- HTTP POST request -----\n");

    HttpRequest *post_req = new HttpRequest(network, HTTP_POST, url);

    post_req->set_header("Content-Type", "application/json");

    // Construction du corps de la requête JSON
    char body[1024]; // Buffer suffisamment grand pour contenir chunkSize valeurs
    snprintf(body, sizeof(body), "{\"values\":[");
    for (int i = 0; i < chunkSize; i++) {
        char valueStr[20];
        sprintf(valueStr, "%0.2f", data[offset + i]);
        strcat(body, valueStr);
        if (i < chunkSize - 1) {
            strcat(body, ",");
        }
    }
    strcat(body, "]}");

    printf("body : %s \n", body );
    HttpResponse *post_res = post_req->send(body, strlen(body));
    if (!post_res) {
        printf("HttpRequest failed (error code %d)\n", post_req->get_error());
        return 1;
    }

    printf("Status: %d - %s\n", post_res->get_status_code(), post_res->get_status_message().c_str());
    printf("Response: %s\n", post_res->get_body_as_string().c_str());

    delete post_req;
    return 0;
}


/**
 * @fn int httpAddSamplesChunkPostRequestv2(const char *url, float data[], int offset, int chunkSize)
 * @brief Méthode pour envoyer un morceau de valeurs en JSON  VERSION2
 * @brief Cette Version envoie également l'@ MAC de la carte pour référencer le système embarqué 
 * @brief Version 2 : Envoie les échantillons sous la forme {mac_address : 00:AC:23:00:10:00,  values : [data1, data2, data3, ..., dataN]}
 * @param const char *url : l'url du web service
 * @param float data[] : Le tableau de données
 * @param offset : la partie des données à envoyer
 * @param chunkSize : la taille des données à envoyer
 * @return void
 */
int httpAddSamplesChunkPostRequestv2(const char *url, float data[], int offset, int chunkSize) {
  //ToDO modifier la fonction conformément à la documentation
    return 0;
}

/**
 * @fn void printSamples(float samples2[])
 * @brief Affiche les samples à l'écran une fois échantilloné
 * @param float samples2[] : le tableau des échantillons
 * @return void
 */
void printSamples(float samples2[]) {
  printf("\nSamples \n");
  for (int i = 0; i < N_SAMPLES; i++) {
    printf("Sample %d: %f g\n", i,
           samples2[i]); // Vérifier les valeurs collectées
  }
  printf("\nEnd Samples \n");
}


/**
 * @fn int main()
 * @brief Fonction principale
 * @return int
 */
int main() {
  printf("\nKey locker intrusion detection program \n");

  DigitalOut led(LED1);
  float sensor_Tvalue;
  float samples[N_SAMPLES];

  printf("Start sensor init\n");
  // Initialiser l'accéléromètre
//   BSP_ACCELERO_Init();
 
 //trace de debug
  mbed_trace_init();
  
  //connection au réseau
  if (NetworkConnectMethod() != 0)
    return -1;
//  collectSamples(samples);

// httpTestSamplePostRequest(add_test_sample_url, 20.5);

//Afficher les échantillons
//  printSamples(samples);

// httpAddSamplesChunkPostRequestv1(add_several_samples_url, samples,0, CHUNK_SIZE);

//SEND SAMPLES CHUNK BY CHUNK

//    int chunkCount = N_SAMPLES / CHUNK_SIZE;
//     for (int i = 0; i < chunkCount; i++) {
//         int result = httpAddSamplesChunkPostRequestv1(add_several_samples_url, samples, i * CHUNK_SIZE, CHUNK_SIZE);
//         if (result != 0) {
//             printf("Failed to send chunk %d\n", i);
//             break;
//         }
//     }


//Boucle désactivée pour les tests
  //   while (true) {
  
  //     ThisThread::sleep_for(BLINKING_RATE);
  //   }
}

